const item_class_ans    = "chat-log__item";
const item_class  	= item_class_ans + " chat-log__item--own"; 
const item_author 	= "chat-log__author";
const item_msg    	= "chat-log__message";

/**
 *	ChatItem constructor
 * */
function ChatItem( name , source , msg , own=true ){
	//var time = $('<small></small>').text(" ahora");
	var author  = $(`<h3 class=\'${item_author}\'></h3>`).text( name );
	var item;
	//author.append( time );
	msg = $(`<div class=\'${item_msg}\'></div>`).text( msg );
	if( own ){
		item = $(`<div class=\'${item_class}\'></div>`).append( author );
	}else{
		item = $(`<div class=\'${item_class_ans}\'></div>`).append( author );
	}
	item.append( source , msg );
	return item;
}

function getAnswer(){
	// Get Question
	var question = $("#pokedexInput").val();
	var chat     = $("#pokedexChat");
	// Show question
	if(question == ""){
		return;
	}
 	$("#pokedexInput").val("");
	chat.append( ChatItem( 'Me' , '' , question ));
	// Make an async query
	$("html").animate({ scrollTop: $(document).height()-$(window).height() });
	$.getJSON( "/pokedex/api/?question="+question , function(data){
		if( data == {} ){
			return chat.append( ChatItem( 'Pokedex' , '' , "Error de comunicaccion" , false ) );
		}
		var items = [];
		for(counter=0; counter<data.default_msg.length; counter++){
			/// Generar un item de chat con los valores del mensaje
			if( data.default_msg[counter].source ){
				items.push( ChatItem( 'Pokedex' , 
					`<img src=\'${data.default_msg[counter].source}\'></img>`, 
					data.default_msg[counter].msg , false ) );
			}else{
				items.push( ChatItem( 'Pokedex','',
					data.default_msg[counter].msg , false));
			}
		}
		chat.append(items);
		///Scroll
		$("html").animate({ scrollTop: $(document).height()-$(window).height() });
	});
	// show answer
}

$(document).ready(function(){
	//onsubmit="getAnswer()"
	$("#pokedexForm").submit( getAnswer );
});

