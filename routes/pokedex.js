var express = require('express');
var router = express.Router();
const superagent = require('superagent');

/* RENDER GUI */
router.get('/', function(req, res, next) {
  res.render('pokedex_gui', { title: 'Pokedex' });
});

/* RESOLVE POKEDEX QUESTIONS */
router.get('/api/',
	function (req , res , next){
		/// Procesar con Hermes
		HermesQuery( req.query.question , 0 , 
		function(qError , qRes){
			/// @HERMES QUERY ERROR
			/// Si ha habido un error, marcar en la estructura y contestar
			if( qError ){ return res.send( {} ); }
			/// Generar la estructura de respuesta
			var preAns = AnswerPreProcessor( qRes.body );
			//res.send(preAns);
			/// Solicitar datos de pokemones
			return PokemonMultyQuery( preAns.params ,
			function(qRes){
				/// @POKEAPI MULTY QUERY ERROR
				/// Si ha habido un error, marcar en la estructura y contestar
				preAns.params = qRes;
				//res.send( preAns );
				res.send( AnswerProcessor( preAns ) );
			});
		});
});

/*
 *	Hermes answer pre-processor
 *	@param	qResponse		superagent response body structure.
 *	@return	PreProcessorStruct	msg to parse, intent.
 * */
function AnswerPreProcessor( qResponse ){
	console.log("\n\r Respuesta por defecto del servidor: ");
	console.log(qResponse.current_response.default_answer+"\n\r");
	var struct = { 	msg:qResponse.current_response.default_answer,
			intent:qResponse.current_response.intent_name,
			params:qResponse.current_response.parameters
			};
	struct.msg = struct.msg.replace(/<(?:.|\n)*?>/gm,'');
	return struct;
}

/*	
 *	PokeAPI Multy Query Handler
 *	@params		pokemons array, with name value
 *	@callback	callback with params (error, ans) where
 *			ans contain all query answers, error is
 *			boolean true for error
 * */
function PokemonMultyQuery( params , callback ){
	var pend;
	try{
		pend = params.entities.length;	// Pending queries.
	}catch( error ){
		/// No parameters received
		return callback( { pokemons:[] , nonpokemons:[] } );
	}
	/// just keep entities
	pokemons = [];
	nonpokemons = [];
	params = params.entities;
	/// Async queries, ordered response
	function IndexedPokemonQuery( iquery ){
	  PokemonQuery( params[ iquery ].name , (error , ans )=> {
		var item = {};
		// Un pendiente menos
		pend--;
		if( error ){
			/// Parametro no pokemon
				//item.pokemon = false;
			item.param = params[iquery];
			nonpokemons.push( item );
		}else{
			/// Parametro pokemon
				//item.pokemon = true;
			item.param = {
				name:ans.body.name,
				abilities:ans.body.abilities,
				height:ans.body.height,
				weight:ans.body.weight,
				moves:ans.body.moves,	
				types:ans.body.types,
				stats:ans.body.stats,
				sprites:ans.body.sprites
			};
			pokemons.push( item );
		}
		// Solo si se es el ultimo query llamar a callback
		if(!pend){ callback( { pokemons , nonpokemons } ); }
		// Sino, terminar y dejar al query pendiente que se encargue
	 });
	}

 	/// Generate a Query for every param
	for( query=pend-1 ; query>-1 ; query-- ){
		console.log( params[query].name );///@print pokemon name	
		IndexedPokemonQuery( query );
	}
}

/*
 *	Answer Processor
 *	Receives an PreAns Structure and adds a default_msg
 *	parsing all %s with the right parameters.
 * */
util = require('util');
function AnswerProcessor( ansStruct ){
	var default_msg = [];
	var pokemons = ansStruct.params.pokemons;
	var nonpokemons = ansStruct.params.nonpokemons;

	switch( ansStruct.intent ){
		case 'Altura pokemon':
			/// No hay parametros.
			if( pokemons.length == 0 ){
				/// No se menciono pokemon
				default_msg.push({
					source:'',
					msg:'No reconoci al pokemon en cuestion'
				});
				break;
			}

			var units=0.10;
			var units_name='mts';
			/// Cual es la unidad que se solicita
			if( nonpokemons.length > 0 ){
				switch( nonpokemons[0].param.name ){
					case 'centimetros':
						units = 10;
						units_name = 'cm';
						break;
					case 'milimetros':
						units = 100;
						units_name = 'mm';
						break
					case 'metros':
						units = 0.10;
						units_name = 'mts';
						break;
					case 'pies':
						units = 0.328084;
						units_name = 'ft';
						break;
					case 'pulgadas':
						units = 3.93701;
						units_name = 'inch';
						break;
					default:
						units = 0.10;
						units_name = 'cm';
				}
			}
			/// Hay parametros
			for(counter=0 ; counter<pokemons.length; counter++){
			  /// Si es pokemon
			default_msg.push({
				source : pokemons[counter]
					.param.sprites.front_default,
				msg : util.format( ansStruct.msg,
					pokemons[counter].param.name,
					((pokemons[counter].param.height) * units )
					+' '+units_name
					)
				});
			}
			break;
		case 'Peso pokemon':
			/// No hay parametros.
			if( pokemons.length == 0 ){
				/// No se menciono pokemon
				default_msg.push({
					source:'',
					msg:'No reconoci al pokemon en cuestion'
				});
				break;
			}


			var units=0.10;
			var units_name='kg';
			/// Cual es la unidad que se solicita
			if( nonpokemons.length > 0 ){
				switch( nonpokemons[0].param.name ){
					case 'libras':
						units = 0.10 * 2.20462;/// Kg to Lib factor
						units_name = "libs";
						break;
					default:
						units_name = "kg";
						units = 0.10;
				}
			}
			/// Hay parametros
			for(counter=0 ; counter<pokemons.length; counter++){
			  /// Si es pokemon
			default_msg.push({
				source : pokemons[counter]
					.param.sprites.front_default,
				msg : util.format( ansStruct.msg,
					pokemons[counter].param.name,
					((pokemons[counter].param.weight) * units )
					+' '+units_name
					)
				});
			}
			break;
		case 'Habilidades pokemon':
			/// No hay parametros.
			if( pokemons.length == 0 ){
				/// No se menciono pokemon
				default_msg.push({
					source:'',
					msg:'No reconoci al pokemon en cuestion'
				});
				break;
			}


			/// Hay parametros
			for(counter=0 ; counter<pokemons.length; counter++){
			  /// Si es pokemon
				/// El numero total de movimientos
				var totalmoves = pokemons[counter].param.abilities.length;
				default_msg.push({
				source : pokemons[counter].param.sprites.front_default,
				msg : util.format( ansStruct.msg,
					pokemons[counter].param.name,
					pokemons[counter].param
						.abilities[ getRandomInt( 0 , totalmoves  ) ]
						.ability
						.name
					)
				});
			}

						break;
		case 'Estadisticas pokemon':
			/// No hay parametros.
			if( pokemons.length == 0 ){
				/// No se menciono pokemon
				default_msg.push({
					source:'',
					msg:'No reconoci al pokemon en cuestion'
				});
				break;
			}


			/// Hay parametros
			for(counter=0 ; counter<pokemons.length; counter++){
				/// Elegir al azar una estadistica
				var totalmoves = pokemons[counter].param.stats.length;
				var totalmoves = getRandomInt( 0 , totalmoves );
			/// Agregar nombre y puntos
			default_msg.push({
				source : pokemons[counter].param.sprites.front_default,
				msg : util.format( ansStruct.msg,
					pokemons[counter].param.name,
					pokemons[counter].param
						.stats[totalmoves]
						.base_stat
					+ ' puntos de ' +
					pokemons[counter].param
						.stats[totalmoves]
						.stat
						.name
					)
				});
			}
						break;
		case 'Movimientos pokemon':
			/// No hay parametros.
			if( pokemons.length == 0 ){
				/// No se menciono pokemon
				default_msg.push({
					source:'',
					msg:'No reconoci al pokemon en cuestion'
				});
				break;
			}


			/// Hay parametros
			for(counter=0 ; counter<pokemons.length; counter++){
				/// El numero total de movimientos
				var totalmoves = pokemons[counter].param.moves.length;
				default_msg.push({
				source : pokemons[counter].param.sprites.front_default,
				msg : util.format( ansStruct.msg,
					pokemons[counter].param.name,
					pokemons[counter].param
						.moves[ getRandomInt( 0 , totalmoves  ) ]
						.move
						.name
					)
				});
			}
						break;
		case 'Tipo pokemon':
			/// No hay parametros.
			if( pokemons.length == 0 ){
				/// No se menciono pokemon
				default_msg.push({
					source:'',
					msg:'No reconoci al pokemon en cuestion'
				});
				break;
			}


			/// Hay parametros
			for(counter=0 ; counter<pokemons.length; counter++){
				/// numero total de tipos
				var totalmoves = pokemons[counter].param.types.length;
				default_msg.push({
				source : pokemons[counter].param.sprites.front_default,
				msg : util.format( ansStruct.msg,
					pokemons[counter].param.name,
					pokemons[counter].param
						.types[ getRandomInt( 0 , totalmoves  ) ]
						.type
						.name
					)
				});
			}
					break;
		default:
			default_msg = [{
				source:'',
				msg:ansStruct.msg
			}];
	}
	ansStruct.default_msg = default_msg;
	return ansStruct;
}
/*
 *	Generador de enteros aleatorios dentro de un rango definido
 * */
function getRandomInt( min , max ){
	return Math.floor( Math.random() * (max-min) + min );
}

/*
 *	SoldAI Hermes Query Handler
 * */
const HermesKEY = '645f82d22c064773a76f237f8d9fd89bc1578256';
function HermesQuery( quest , id , callback  ){
	superagent.get('https://beta.soldai.com//bill-cipher/askquestion')
	.query({session_id:id,
		key: HermesKEY,
		log:'1',
		question:quest })
	.end( callback	);
}

/*
 *	PokeAPI Query Handler
 * */
function PokemonQuery( name , callback ){
	superagent.get(`https://pokeapi.co/api/v2/pokemon/${name}/`)
	.query()
	.end( callback );
}

module.exports = router;
