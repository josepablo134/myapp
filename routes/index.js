var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});


const superagent = require('superagent');

/*Get Test page*
	REQUEST NASA API PAGE
	*/
router.get( '/Test/',
	function (req , res, next){
		superagent.get('https://api.nasa.gov/planetary/apod')
			.query({ api_key: 'DEMO_KEY' })
			.end(	(qErr, qRes) => {
					if (qErr) { return res.send(' Query Erroy '); /*return console.log(qErr);*/ }
					//console.log(res.body.url);
					//console.log(res.body.explanation);
					res.send( qRes.body );
				}
			);
		//res.send( 'Hello world!\n' );
	}
);

/* Poke Api Query */
/// Toda la informacion del pokemon
router.get( '/pokemon/:pokemon' ,
	function (req , res , next){
		console.log(`pokemon requested : ${req.params.pokemon}`);
		superagent.get(`https://pokeapi.co/api/v2/pokemon/${req.params.pokemon}/`)
		.query()
		.end(
			function (qError , qRes){
				if(qError){ return res.send("Query Error"); }

				res.send( qRes.body );
			}
		);
	}
);

router.get('/soldai/',
	function (req , res , next){
		console.log(`Question requested : ${req.query.question}`);
		superagent.get('https://beta.soldai.com//bill-cipher/askquestion')
		.query({session_id:'0',
			key:'645f82d22c064773a76f237f8d9fd89bc1578256',
			log:'1',
			question : req.query.question
		})
		.end(
			function (qError , qRes){
				if(qError){ return res.send("Query Error"); }

				res.send( qRes.body );
			}
		);
	}
);

module.exports = router;
